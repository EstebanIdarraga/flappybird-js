const game = new Phaser.Game(330, 510, Phaser.CANVAS, 'bloque_juego');

let fondoJuego;
let boton;
let personaje;
let suelo;
var controles;
var persona;
var estadoPersona = 'abajo';


var estadoPrincipal = {
    preload: function(){
        //Esta funcion Carga todos los recursos -> Imagenes, sonidos etc...  
        // game.stage.backgroundColor = '#000';    color del juego
         game.load.image('fondo', 'img/background-night.png');
         game.load.spritesheet('pajaros', 'img/pajaros.png', 35.3, 25);     
         game.load.image('btn', 'img/message.png');
         game.load.image('suelo', 'img/base.png');

         

         game.load.spritesheet('persona', 'img/sprite.png', 68, 72);
    },
    create: function(){
        //Mostar recursos que trae la funcion preload
       fondoJuego =  game.add.tileSprite(0, 0, 370, 550, 'fondo');

      /* suelo = game.add.sprite(0, 400, 'suelo');

       personaje = game.add.sprite(100, 100, 'pajaros');
        
       personaje.frame = 1;
       
       personaje.animations.add('vuelo', [0, 1, 2], 5, true);*/
       persona = game.add.sprite(game.width/2, game.height/2, 'persona');
       persona.anchor.setTo(0.5);

       persona.animations.add('abajo', [0,1,2,3], 5, true);
       persona.animations.add('izquierda', [4,5,6,7], 5, true);
       persona.animations.add('derecha', [8,9,10,11], 5, true);
       persona.animations.add('arriba', [12,13,14,15], 5, true);

       controles = game.input.keyboard.createCursorKeys();


       game.physics.startSystem(Phaser.Physics.ARCADE);


       game.physics.arcade.enable(persona);
       persona.body.collideWorldBounds = true;

      /* game.physics.arcade.enable(personaje);

       personaje.body.collideWorldBounds = true;*/
      
       //teclaDerecha = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);


  /*   personaje = game.add.sprite(game.width/2,game.height/2, 'pajaro');
        
       personaje.anchor.seTo(0.5);
       personaje.scale.setTo(1);
        
       boton = game.add.sprite(game.width/2, game.height/2, 'btn');
               
       boton.anchor.setTo(0.5);*/
         
    },
    update: function(){
        //Animamos el juego
        //fondoJuego.tilePosition.x -= 1; //Movimiento del fondo
        // personaje.angle += 0.5; Para rotar el personaje

        //personaje.animations.play('vuelo');

        if(controles.right.isDown){
            //personaje.position.x += 1 ;
            persona.position.x += 1;
            persona.animations.play('derecha');

            if(estadoPersona != 'derecha'){
                estadoPersona = 'derecha';
            }

        }
       else if(controles.left.isDown){
            //personaje.position.x -= 1 ;
            persona.position.x -= 1;
            persona.animations.play('izquierda');

            if(estadoPersona != 'izquierda'){
                estadoPersona = 'izquierda';
            }
        }
       else if(controles.up.isDown){
            //personaje.position.y -= 1 ;
            persona.position.y -=1;
            persona.animations.play('arriba');

            if(estadoPersona != 'arriba'){
                estadoPersona = 'arriba';
            }
            
        }
        else if(controles.down.isDown){
            //personaje.position.y += 1 ;
            persona.position.y += 1;
            persona.animations.play('abajo');

            if(estadoPersona != 'abajo'){
                estadoPersona = 'abajo';
            }
        }
        else{
            if(estadoPersona != 'espera'){
                persona.animations.stop();
            }

            estadoPersona = 'espera';
        }

        
    }
};


game.state.add('principal',estadoPrincipal);
game.state.start('principal');